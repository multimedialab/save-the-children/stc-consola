import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const UsersView = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './users-view')
);
const Users = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/main`} />
      <Route
        path={`${match.url}/main`}
        render={(props) => <UsersView {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default Users;
