/* eslint-disable no-param-reassign */
/* eslint no-unused-expressions: ["error", { "allowTernary": true }] */
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Row } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import { DominaTable } from 'containers/ui/ReactTableCards';
import Breadcrumb from 'containers/navs/Breadcrumb';
import ExportExcel from 'components/exportExcel/ExportExcel';
import { loadUsersRequested } from '../../../redux/actions';

const UserView = ({ match }) => {
  const dispatch = useDispatch();
  const { dataUsers } = useSelector(state => state.users);
  // const { isLoading } = useSelector(state => state.users);
  // const { error } = useSelector(state => state.users);

  useEffect(() => {
    dispatch(loadUsersRequested());
  }, []);

  const headers = [
      {
        Header: 'ID',
        accessor: 'id',
        cellClass: 'list-item-heading w-15',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'NOMBRES',
        accessor: 'username',
        cellClass: 'text-muted  w-10',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'APELLIDOS',
        accessor: 'lastname',
        cellClass: 'text-muted  w-10',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'EDAD',
        accessor: 'age',
        cellClass: 'text-muted  w-10',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'CLAVE',
        accessor: 'code',
        cellClass: 'text-muted  w-20',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'PAIS',
        accessor: 'pais',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'REGIÓN',
        accessor: 'region',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
  ]

  return(
    <>
      <Row>
        <Colxx xxs="12">
          <Row>
            <Colxx xxs="6">
              <Breadcrumb heading="menu.users" match={match} />
            </Colxx>
          </Row>
          
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <div>
        
        <ExportExcel headers= { headers } data= { dataUsers } filename= 'Users'/>
      
    </div>
      <Row>
        <Colxx xxs="12">
            <DominaTable cols={headers} data={dataUsers}/>{' '}
        </Colxx>
      </Row>
    </>
  )
};
export default UserView;
