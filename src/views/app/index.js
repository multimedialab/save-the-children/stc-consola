import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from 'layout/AppLayout';

const Panel = React.lazy(() =>
  import(/* webpackChunkName: "viwes-gogo" */ './panel')
);
const Users = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './users')
);
const Progress = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './progress')
);
const Evaluations = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './evaluations')
);
const Polls = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './polls')
);
const Settings = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './settings')
);
// const BlankPage = React.lazy(() =>
//   import(/* webpackChunkName: "viwes-blank-page" */ './blank-page')
// );

const App = ({ match }) => {
  return (
    <AppLayout>
      <div className="dashboard-wrapper">
        <Suspense fallback={<div className="loading" />}>
          <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/panel`} />
            <Route
              path={`${match.url}/panel`}
              render={(props) => <Panel {...props} />}
            />
            <Route
              path={`${match.url}/users`}
              render={(props) => <Users {...props} />}
            />
            <Route
              path={`${match.url}/progress`}
              render={(props) => <Progress {...props} />}
            />
            <Route
              path={`${match.url}/evaluations`}
              render={(props) => <Evaluations {...props} />}
            />
             <Route
              path={`${match.url}/polls`}
              render={(props) => <Polls {...props} />}
            />
            <Route
              path={`${match.url}/settings`}
              render={(props) => <Settings {...props} />}
            />
            {/* <Route
              path={`${match.url}/blank-page`}
              render={(props) => <BlankPage {...props} />}
            /> */}
            <Redirect to="/error" />
          </Switch>
        </Suspense>
      </div>
    </AppLayout>
  );
};

const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(App));
