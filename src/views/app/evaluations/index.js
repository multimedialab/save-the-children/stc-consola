import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const EvaluationsView = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './evaluations-view')
);
const Evaluations = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/main`} />
      <Route
        path={`${match.url}/main`}
        render={(props) => <EvaluationsView {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default Evaluations;