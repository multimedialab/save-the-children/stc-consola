import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const PanelView = React.lazy(() =>
  import(/* webpackChunkName: "start" */ './panel-view')
);
const Panel = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/analytics`} />
      <Route
        path={`${match.url}/analytics`}
        render={(props) => <PanelView {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default Panel;
