/* eslint-disable no-param-reassign */
/* eslint no-unused-expressions: ["error", { "allowTernary": true }] */
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Row } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import { DominaTable } from 'containers/ui/ReactTableCards';
import Breadcrumb from 'containers/navs/Breadcrumb';
import ExportExcel from 'components/exportExcel/ExportExcel';
import { loadProgressRequested } from '../../../redux/actions';

const ProgressView = ({ match }) => {
  const dispatch = useDispatch();
  const { dataProgress } = useSelector(state => state.progress);
  // const { isLoading } = useSelector(state => state.users);
  // const { error } = useSelector(state => state.users);

  useEffect(() => {
    dispatch(loadProgressRequested());
  }, []);

  const headers = [
      {
        Header: 'ID',
        accessor: 'id',
        cellClass: 'list-item-heading w-15',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'NOMBRES',
        accessor: 'name',
        cellClass: 'text-muted  w-20',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'APELLIDOS',
        accessor: 'lastname',
        cellClass: 'text-muted  w-20',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'TOTAL',
        accessor: 'total',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'NV 1',
        accessor: 'nivel1',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'NV 2',
        accessor: 'nivel2',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'NV 3',
        accessor: 'nivel3',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'NV 4',
        accessor: 'nivel4',
        cellClass: 'text-muted  w-10',
        // Cell: (props) => <>{props.value}</>,
      },
  ]

  return(
    <>
      <Row>
        <Colxx xxs="12">
          <Row>
            <Colxx xxs="6">
              <Breadcrumb heading="menu.progress" match={match} />
            </Colxx>
          </Row>
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <div>
        
        <ExportExcel headers= { headers } data= { dataProgress } filename= 'Progress'/>
      
    </div>
      <Row>
        <Colxx xxs="12">
            <DominaTable cols={headers} data={dataProgress}/>{' '}
        </Colxx>
      </Row>
    </>
  )
};
export default ProgressView;
