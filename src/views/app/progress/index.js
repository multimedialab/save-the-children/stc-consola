import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const ProgressView = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './progress-view')
);
const Progress = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/main`} />
      <Route
        path={`${match.url}/main`}
        render={(props) => <ProgressView {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default Progress;
