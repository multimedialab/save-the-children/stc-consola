import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const PollsView = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './polls-view')
);
const Polls = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/main`} />
      <Route
        path={`${match.url}/main`}
        render={(props) => <PollsView {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default Polls;