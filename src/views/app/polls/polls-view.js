/* eslint-disable no-param-reassign */
/* eslint no-unused-expressions: ["error", { "allowTernary": true }] */
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Row } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import { DominaTable } from 'containers/ui/ReactTableCards';
import Breadcrumb from 'containers/navs/Breadcrumb';
import ExportExcel from 'components/exportExcel/ExportExcel';
import { loadPollsRequested } from '../../../redux/actions';
/* import {ExportExcel} from 'react-export-excel'; */
/* import ReactHTMLTableToExcel from 'react-html-table-to-excel'; */



const PollsView = ({ match }) => {
  const dispatch = useDispatch();
  const { dataPolls } = useSelector(state => state.polls);
  /* const ExcelFile = ExportExcel.ExcelFile;
  const ExcelSheet = ExportExcel.ExcelSheet;
  const ExcelColumn = ExportExcel.ExcelColumn; */
  // const { isLoading } = useSelector(state => state.users);
  // const { error } = useSelector(state => state.users);

  useEffect(() => {
    dispatch(loadPollsRequested());
  }, []);

  const headers = [
      
    {
      Header: 'CODIGO',
      accessor: 'codigo',
      cellClass: 'text-muted  w-10',
      // Cell: (props) => <>{props.value}</>,
    },

    {
        Header: 'NOMBRES',
        accessor: 'name',
        cellClass: 'text-muted  w-15',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'APELLIDOS',
        accessor: 'lastname',
        cellClass: 'text-muted  w-15',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'FECHA',
        accessor: 'fechaparse',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'RESPUESTA 1',
        accessor: 'respuesta1',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'RESPUESTA 2',
        accessor: 'respuesta2',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'RESPUESTA 3',
        accessor: 'respuesta3',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'PAIS',
        accessor: 'pais',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'REGIÓN',
        accessor: 'region',
        cellClass: 'text-muted  w-5',
        // Cell: (props) => <>{props.value}</>,
      },
      
  ]

  return(
    <>
    
      
      <Row>
        <Colxx xxs="12">
          <Row>
            <Colxx xxs="6">
              <Breadcrumb heading="menu.polls" match={match} />
            </Colxx>
          </Row>
          <Separator className="mb-5" />
        </Colxx>
      </Row>
    <div>
        
        <ExportExcel headers= { headers } data= { dataPolls } filename= 'Polls'/>
      
    </div>
      <Row>
        <Colxx xxs="12">
            <DominaTable cols={headers} data={dataPolls}/>{' '}
        </Colxx>
      </Row>

     
    </>
    
  );
};
export default PollsView;
