
import React from "react";
import ReactExport from "react-export-excel";
import { Button } from 'reactstrap';

const { ExcelFile } = ReactExport;
const { ExcelSheet } = ExcelFile;
const { ExcelColumn } = ExcelFile;

const ExportExcel = ( { headers, data, filename } ) => {

    return (

        <ExcelFile filename={ filename } element={<Button outline color="primary">Exportar a Excel</Button>}>
            <ExcelSheet data={ data }  name="Employees">
             {
                 headers.map((value) => (
                    <ExcelColumn key={ value.accessor } label={ value.Header } value={ value.accessor }/>
                 ))
             }
            </ExcelSheet>
        </ExcelFile>
    );
}

export default ExportExcel;