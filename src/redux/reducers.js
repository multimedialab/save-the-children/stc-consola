import { combineReducers } from 'redux';
import settings from './settings/reducer';
import menu from './menu/reducer';
import users from './miusers/reducer';
import progress from './miprogress/reducer';
import evaluations from './mievaluations/reducer';
import polls from './mipolls/reducer';

const reducers = combineReducers({
  menu,
  settings,
  users,
  progress,
  evaluations,
  polls
});

export default reducers;
