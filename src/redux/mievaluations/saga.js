import { convertDate } from 'helpers/Utils';
import { call, put, takeEvery } from 'redux-saga/effects';
import { database } from '../../helpers/Firebase';
import { LOAD_EVALUATIONS_REQUESTED } from '../actions';
import { loadEvaluationsError, loadEvaluationsSuccess } from './actions';

const getFirestoreEvaluations = async () => {
    const data = await database.collection("evaluations")
    .orderBy('lastname', 'desc')
    .get()
    .then((querySnapshot) => {
        const tempArray = [];
        querySnapshot.forEach((doc) => {
            tempArray.push({          
              ...doc.data(),
              fechaparse: convertDate(doc.data().fecha)
            });
        });
        return tempArray;
    })
    .catch((error) => {
        console.log("Error getting documents: ", error);
        return false;
    });

    return data;
}

function* getEvaluations () {
    try {
        const users = yield call(getFirestoreEvaluations);
        if (users.length > 0) {
            yield put(loadEvaluationsSuccess(users));
        }else{
            throw new Error('Error al obtener documentos');
        }

    } catch (error) {
        yield put(loadEvaluationsError('Error al obtener documentos'));
    }
}

function* evaluationsSaga(){
    yield takeEvery(LOAD_EVALUATIONS_REQUESTED, getEvaluations);
}

export default evaluationsSaga;