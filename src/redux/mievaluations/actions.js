/* eslint-disable import/no-cycle */
/* eslint-disable no-param-reassign */
import {  LOAD_EVALUATIONS_ERROR, LOAD_EVALUATIONS_REQUESTED, LOAD_EVALUATIONS_SUCCESS } from '../actions';

export const loadEvaluationsRequested = () => ({
    type: LOAD_EVALUATIONS_REQUESTED,
});

export const loadEvaluationsSuccess = ( dataEvaluations ) => ({
    type: LOAD_EVALUATIONS_SUCCESS,
    payload: dataEvaluations
});

export const loadEvaluationsError = ( msg ) => ({
    type: LOAD_EVALUATIONS_ERROR,
    payload: msg
});