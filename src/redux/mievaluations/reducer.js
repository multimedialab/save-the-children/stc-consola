import { LOAD_EVALUATIONS_ERROR, LOAD_EVALUATIONS_REQUESTED, LOAD_EVALUATIONS_SUCCESS } from '../actions';

const INIT_STATE = {
    dataEvaluations: [],
    isLoading: false,
    error: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
      case LOAD_EVALUATIONS_REQUESTED:
        return { ...state, isLoading: true };
      case LOAD_EVALUATIONS_SUCCESS:
        return { ...state, dataEvaluations: action.payload, isLoading: false };
      case LOAD_EVALUATIONS_ERROR:
        return { ...state, error: action.payload, isLoading: false };
      default:
        return { ...state };
    }
};