/* eslint-disable import/no-cycle */
/* eslint-disable no-param-reassign */
import {  LOAD_POLLS_ERROR, LOAD_POLLS_REQUESTED, LOAD_POLLS_SUCCESS } from '../actions';

export const loadPollsRequested = () => ({
    type: LOAD_POLLS_REQUESTED,
});

export const loadPollsSuccess = ( dataPolls ) => ({
    type: LOAD_POLLS_SUCCESS,
    payload: dataPolls
});

export const loadPollsError = ( msg ) => ({
    type: LOAD_POLLS_ERROR,
    payload: msg
});