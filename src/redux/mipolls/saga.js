import { convertDate } from 'helpers/Utils';
import { call, put, takeEvery } from 'redux-saga/effects';
import { database } from '../../helpers/Firebase';
import { LOAD_POLLS_REQUESTED } from '../actions';
import { loadPollsError, loadPollsSuccess } from './actions';

const getFirestorePolls = async () => {
    const data = await database.collection("polls")
    .orderBy('lastname', 'desc')
    .get()
    .then((querySnapshot) => {
        const tempArray = [];
        querySnapshot.forEach((doc) => {
            tempArray.push({          
              ...doc.data(),
              fechaparse: convertDate(doc.data().fecha)
            });
        });
        return tempArray;
    })
    .catch((error) => {
        console.log("Error getting documents: ", error);
        return false;
    });

    return data;
}

function* getPolls () {
    try {
        const users = yield call(getFirestorePolls);
        if (users.length > 0) {
            yield put(loadPollsSuccess(users));
        }else{
            throw new Error('Error al obtener documentos');
        }

    } catch (error) {
        yield put(loadPollsError('Error al obtener documentos'));
    }
}

function* pollsSaga(){
    yield takeEvery(LOAD_POLLS_REQUESTED, getPolls);
}

export default pollsSaga;