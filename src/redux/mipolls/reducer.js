import { LOAD_POLLS_ERROR, LOAD_POLLS_REQUESTED, LOAD_POLLS_SUCCESS } from '../actions';

const INIT_STATE = {
    dataPolls: [],
    isLoading: false,
    error: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
      case LOAD_POLLS_REQUESTED:
        return { ...state, isLoading: true };
      case LOAD_POLLS_SUCCESS:
        return { ...state, dataPolls: action.payload, isLoading: false };
      case LOAD_POLLS_ERROR:
        return { ...state, error: action.payload, isLoading: false };
      default:
        return { ...state };
    }
};