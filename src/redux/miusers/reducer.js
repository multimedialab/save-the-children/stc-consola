import { LOAD_USERS_ERROR, LOAD_USERS_REQUESTED, LOAD_USERS_SUCCESS } from '../actions';

const INIT_STATE = {
    dataUsers: [],
    isLoading: false,
    error: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
      case LOAD_USERS_REQUESTED:
        return { ...state, isLoading: true };
      case LOAD_USERS_SUCCESS:
        return { ...state, dataUsers: action.payload, isLoading: false };
      case LOAD_USERS_ERROR:
        return { ...state, error: action.payload, isLoading: false };
      default:
        return { ...state };
    }
};