/* eslint-disable import/no-cycle */
/* eslint-disable no-param-reassign */
import { LOAD_USERS_ERROR, LOAD_USERS_REQUESTED, LOAD_USERS_SUCCESS } from '../actions';

export const loadUsersRequested = () => ({
    type: LOAD_USERS_REQUESTED,
});

export const loadUsersSuccess = ( dataUsers ) => ({
    type: LOAD_USERS_SUCCESS,
    payload: dataUsers
});

export const loadUsersError = ( msg ) => ({
    type: LOAD_USERS_ERROR,
    payload: msg
});