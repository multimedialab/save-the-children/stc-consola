import { calculateYears } from 'helpers/Utils';
import { call, put, takeEvery } from 'redux-saga/effects';
import { database } from '../../helpers/Firebase';
import { LOAD_USERS_REQUESTED } from '../actions';
import { loadUsersError, loadUsersSuccess } from './actions';

const getFirestoreUsers = async () => {
    const data = await database.collection("users")
    .orderBy('username', 'desc')
    .get()
    .then((querySnapshot) => {
        const tempArray = [];
        querySnapshot.forEach((doc) => {
            tempArray.push({
              id: doc.id,
              age: doc.data().fechanacimiento ? calculateYears(doc.data().fechanacimiento) : '',
              ...doc.data(),
            });
        });
        return tempArray;
    })
    .catch((error) => {
        console.log("Error getting documents: ", error);
        return false;
    });

    return data;
}

function* getUsers () {
    try {
        const users = yield call(getFirestoreUsers);
        if (users.length > 0) {
            yield put(loadUsersSuccess(users));
        }else{
            throw new Error('Error al obtener documentos');
        }

    } catch (error) {
        yield put(loadUsersError('Error al obtener documentos'));
    }
}

function* userSaga(){
    yield takeEvery(LOAD_USERS_REQUESTED, getUsers);
}

export default userSaga;