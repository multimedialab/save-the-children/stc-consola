import { all } from 'redux-saga/effects';
import authSagas from './auth/saga';
import progresSaga from './miprogress/saga';
import userSaga from './miusers/saga';
import evaluationsSaga from './mievaluations/saga';
import pollsSaga from './mipolls/saga';

export default function* rootSaga() {
  yield all([
    authSagas(),
    userSaga(),
    progresSaga(),
    evaluationsSaga(),
    pollsSaga()
  ]);
}
