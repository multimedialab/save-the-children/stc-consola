import { LOAD_PROGRESS_ERROR, LOAD_PROGRESS_REQUESTED, LOAD_PROGRESS_SUCCESS } from '../actions';

const INIT_STATE = {
    dataProgress: [],
    isLoading: false,
    error: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
      case LOAD_PROGRESS_REQUESTED:
        return { ...state, isLoading: true };
      case LOAD_PROGRESS_SUCCESS:
        return { ...state, dataProgress: action.payload, isLoading: false };
      case LOAD_PROGRESS_ERROR:
        return { ...state, error: action.payload, isLoading: false };
      default:
        return { ...state };
    }
};