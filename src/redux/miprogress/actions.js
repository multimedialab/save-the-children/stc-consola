/* eslint-disable import/no-cycle */
/* eslint-disable no-param-reassign */
import { LOAD_PROGRESS_ERROR, LOAD_PROGRESS_REQUESTED, LOAD_PROGRESS_SUCCESS } from '../actions';

export const loadProgressRequested = () => ({
    type: LOAD_PROGRESS_REQUESTED,
});

export const loadProgresSuccess = ( dataProgress ) => ({
    type: LOAD_PROGRESS_SUCCESS,
    payload: dataProgress
});

export const loadProgressError = ( msg ) => ({
    type: LOAD_PROGRESS_ERROR,
    payload: msg
});