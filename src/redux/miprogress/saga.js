import { call, put, takeEvery } from 'redux-saga/effects';
import { database } from '../../helpers/Firebase';
import { LOAD_PROGRESS_REQUESTED } from '../actions';
import { loadProgressError, loadProgresSuccess } from './actions';

const getFirestoreProgress = async () => {
    const data = await database.collection("progress")
    .orderBy('name', 'desc')
    .get()
    .then((querySnapshot) => {
        const tempArray = [];
        querySnapshot.forEach((doc) => {
            const { nivel1, nivel2, nivel3, nivel4} = doc.data();

            tempArray.push({
              id: doc.id,
              total: `${((parseInt(nivel1, 10)+parseInt(nivel2, 10)+parseInt(nivel3, 10)+parseInt(nivel4, 10))*100)/40} %`,
              ...doc.data(),
            });
        });
        return tempArray;
    })
    .catch((error) => {
        console.log("Error getting documents: ", error);
        return false;
    });

    return data;
}

function* getProgress () {
    try {
        const users = yield call(getFirestoreProgress);
        if (users.length > 0) {
            yield put(loadProgresSuccess(users));
        }else{
            throw new Error('Error al obtener documentos');
        }

    } catch (error) {
        yield put(loadProgressError('Error al obtener documentos'));
    }
}

function* progresSaga(){
    yield takeEvery(LOAD_PROGRESS_REQUESTED, getProgress);
}

export default progresSaga;