/* Gogo Language Texts

Table of Contents

01.General
02.User Login, Logout, Register
03.Menu
04.Error Page
*/

module.exports = {
  /* 01.General */
  'general.copyright': 'Gogo React © Todos los derechos reservados.',

  /* 02.Inicio de sesión de usuario, cierre de sesión, registro */
  'user.login-title': 'Iniciar sesión',
  'user.register': 'Registro',
  'user.forgot-password': 'Se te olvidó tu contraseña',
  'user.email': 'Email',
  'user.password': 'Contraseña',
  'user.forgot-password-question': '¿Contraseña olvidada?',
  'user.fullname': 'Nombre completo',
  'user.login-button': 'INICIAR SESIÓN',
  'user.register-button': 'REGISTRO',
  'user.reset-password-button': 'REINICIAR',
  'user.buy': 'COMPRAR',
  'user.username': 'Nombre de Usuario',

  /* 03.Menú */
  'menu.home': 'Inicio',
  'menu.app': 'Inicio',
  'menu.dashboards': 'Tableros',
  'menu.panel': 'Panel',
  'menu.start': 'Comienzo',
  'menu.second': 'Segundo',
  'menu.ui': 'IU',
  'menu.charts': 'Gráficos',
  'menu.chat': 'Chatea',
  'menu.survey': 'Encuesta',
  'menu.todo': 'Notas',
  'menu.search': 'Búsqueda',
  'menu.docs': 'Docs',
  'menu.users': 'Usuarios',
  'menu.progress': 'Progresos',
  'menu.settings': 'Configuracion',
  'menu.main': 'Principal',
  'menu.analytics': 'Análisis',
  'menu.evaluations': 'Evaluación',
  'menu.polls': 'Encuesta',

  /* 05.Pages */
  'pages.add-new': 'AGREGAR NUEVO',

  /* 04.Tableros */
  'dashboards.pending-orders': 'Pedidos Pendientes',
  'dashboards.completed-orders': 'Pedidos Completados',
  'dashboards.refund-requests': 'Petición de Reembolso',
  'dashboards.new-comments': 'Nuevos Comentarios',
  'dashboards.sales': 'Ventas',
  'dashboards.orders': 'Pedidos',
  'dashboards.refunds': 'Reembolso',
  'dashboards.recent-orders': 'Pedidos Recientas',
  'dashboards.product-categories': 'Categorías de Producto',
  'dashboards.cakes': 'Tortas',
  'dashboards.tickets': 'Entradas',
  'dashboards.calendar': 'Calendario',
  'dashboards.best-sellers': 'Mejores Vendidos',
  'dashboards.website-visits': 'Visitas al sitio web',
  'dashboards.unique-visitors': 'Visitantes únicos',
  'dashboards.this-week': 'Esta Semana',
  'dashboards.last-week': 'La Semana Pasada',
  'dashboards.this-month': 'Este Mes',
  'dashboards.conversion-rates': 'Medidas de Conversión',
  'dashboards.per-session': 'Por Sección',
  'dashboards.profile-status': 'Estado del Perfil',
  'dashboards.payment-status': 'Estado del Pago',
  'dashboards.work-progress': 'Trabajo en Progreso',
  'dashboards.tasks-completed': 'Tareas Completadas',
  'dashboards.payments-done': 'Pagos Realizados',
  'dashboards.order-stock': 'Pedidos - Valores',
  'dashboards.categories': 'Categorías',
  'dashboards.quick-post': 'Publicación Rápida',
  'dashboards.title': 'Título',
  'dashboards.content': 'Contenido',
  'dashboards.category': 'Categoría',
  'dashboards.save-and-publish': 'Guardar y Publicar',
  'dashboards.top-viewed-posts': 'Publicaciones Más Vistas',
  'dashboards.posts': 'Puestos',
  'dashboards.pending-for-publish': 'Pendiente de Publicación',
  'dashboards.users': 'Usuarios',
  'dashboards.on-approval-process': 'En Proceso de Aprobación',
  'dashboards.alerts': 'Alertas',
  'dashboards.waiting-for-notice': 'Esperando Aviso',
  'dashboards.files': 'Archivos',
  'dashboards.pending-for-print': 'Pendiente para imprimir',
  'dashboards.logs': 'Troncos',
  'dashboards.gogo': 'GOGO',
  'dashboards.magic-is-in-the-details': 'LA MAGIA ESTA EN LOS DETALLES',
  'dashboards.yes-it-is-indeed': '¡Si es verdad!',
  'dashboards.advanced-search': 'Búsqueda Avanzada',
  'dashboards.toppings': 'Coberturas',
  'dashboards.type': 'Categoría',
  'dashboards.keyword': 'Palabra Clave',
  'dashboards.search': 'Búsqueda',
  'dashboards.top-rated-items': 'Artículos Mejores Valorados',

  /* 04.Error  */
  'layouts.error-title': 'Vaya, parece que ha ocurrido un error!',
  'layouts.error-code': 'Código de error',
  'layouts.go-back-home': 'REGRESAR A INICIO',
};
