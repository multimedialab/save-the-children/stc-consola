import { adminRoot } from './defaultValues';
// import { UserRole } from "helpers/authHelper"

const data = [
  {
    id: 'panel',
    icon: 'iconsminds-digital-drawing',
    label: 'menu.panel',
    to: `${adminRoot}/panel`,
    // roles: [UserRole.Admin, UserRole.Editor],
  },
  {
    id: 'users',
    icon: 'simple-icon-people',
    label: 'menu.users',
    to: `${adminRoot}/users`,
  },
  {
    id: 'progress',
    icon: 'simple-icon-trophy',
    label: 'menu.progress',
    to: `${adminRoot}/progress`,
  },
  {
    id: 'evaluations',
    icon: 'simple-icon-note',
    label: 'menu.evaluations',
    to: `${adminRoot}/evaluations`,
  },
  {
    id: 'polls',
    icon: 'simple-icon-chart',
    label: 'menu.polls',
    to: `${adminRoot}/polls`,
  },
];
export default data;
