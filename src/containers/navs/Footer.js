import React from 'react';

const Footer = () => {
  return (
    <footer className="page-footer">
      <div className="footer-content">
        <div className="container-fluid"/>
      </div>
    </footer>
  );
};

export default Footer;
